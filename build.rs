use std::collections::HashMap;
use std::fs;
use std::env;
use std::io;
use std::path::Path;
use serde_derive::{Serialize,Deserialize};
use minreq;
const CURRENCY_API_KEY: &str = env!("CURAPIKEY");

#[derive(Debug,Serialize,Deserialize,Clone)]
struct Currency{
    currency_name:String,
    rate:String,
    rate_for_amount:String
}

impl Currency{
    pub fn print(&self) -> String{
        format!("Currency{{currency_name:\"{}\",rate:{},rate_for_amount:{}}}",self.currency_name, self.rate, self.rate_for_amount)
    }
}

#[derive(Debug,Serialize,Deserialize,Clone)]
struct ConvertData{
    base_currency_code: String,
    base_currency_name: String,
    amount: String,
    updated_date: String,
    pub rates: HashMap<String,Currency>
}

impl Default for ConvertData{
    fn default() -> Self {
        ConvertData{
            base_currency_code: "USD".to_string(),
            base_currency_name: "USA Dollar".into(),
            amount: String::from("1.0"),
            updated_date: String::new(),
            rates: HashMap::from([
                ("USD".to_string(),Currency{
                        currency_name: "USA Dollar".into(),
                        rate: "1.0".to_string(),
                        rate_for_amount: "1.0".to_string(),
                    }
                ),
                ("PLN".to_string(),Currency{
                        currency_name: "Polish zloty".into(),
                        rate: "4.5".to_string(),
                        rate_for_amount: "4.5".to_string(),
                    }
                ),
            ])
        }
    }
}

impl ConvertData{
    pub fn print_and_forget(self) -> String{
        self.rates
            .iter()
            .map(|(k,v)|format!("\"{}\" => {}",k,v.print()))
            .collect::<Vec<String>>()
            .join(",\n")
    }
}

fn request_data() -> ConvertData
{
    let uri = format!("https://api.getgeoapi.com/v2/currency/convert?api_key={}&from=USD&format=json",CURRENCY_API_KEY);
    let req = minreq::get(uri)
        .with_header("Content-Type", "application/json")
    ;
    if let Ok(res) = req.send(){
        if res.status_code == 200{
            let val = res.json::<ConvertData>().unwrap();
            val
        }
        else{
            ConvertData::default()
        }
    }
    else{
        ConvertData::default()
    }
}

fn main(){
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("env.rs");
    fs::remove_file(&dest_path)
        .or_else(|_| io::Result::Ok(()))
        .unwrap();
    fs::write(
        &dest_path,
format!("pub static CURRENCY_MAP: Map<&'static str, Currency> = phf_map! {{
    {}
}};",request_data().print_and_forget())
    ).unwrap();
}
