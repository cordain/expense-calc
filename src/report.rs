use std::collections::{HashMap, HashSet};

use leptos::{html::Select, *};

use crate::{
   currency::{Currencies, CurrenciesProps, CurrencyHandle},
   expense::Expense,
};

type CostMatrix = HashMap<String, HashMap<String, f32>>;

pub fn calculate(
   people: &HashSet<String>,
   expenses: &Vec<Expense>,
   currency_handler: &CurrencyHandle,
   target_currency: String,
) -> Result<Vec<(String, String, f32)>, ()> {
   //init cost matrix
   let mut cost_matrix: CostMatrix;
   cost_matrix = HashMap::new();
   //get target currency factor
   let target_currency_factor = currency_handler.get_rate(target_currency);
   for name in people.iter() {
      let mut cost_row: HashMap<String, f32> = HashMap::new();
      for name in people.iter() {
         cost_row.insert(name.clone(), 0f32);
      }

      cost_matrix.insert(name.clone(), cost_row);
   }

   //calculate cost matrix
   for expense in expenses.iter() {
      let base_currency_factor = currency_handler.get_rate(expense.get_currency());
      let expense_report = expense.calculate(base_currency_factor);
      let buyer = expense.get_buyer();
      let buyer_ref = people.get(&buyer).ok_or(())?;
      for (ower, amount) in expense_report {
         //get reference from names vec
         let ower_ref = people.get(&ower).ok_or(())?;

         let cost = cost_matrix
            .get_mut(buyer_ref)
            .ok_or(())?
            .get_mut(ower_ref)
            .ok_or(())?;
         //add cost in target currency
         *cost += amount * target_currency_factor;
      }
   }

   //bubble_optimizer
   for person_left in people {
      for person_right in people {
         if person_left != person_right {
            let cost_left = cost_matrix[person_left][person_right];
            let cost_right = cost_matrix[person_right][person_left];
            if cost_left != 0.0f32 && cost_right != 0.0f32 {
               let diff = if cost_left < cost_right {
                  cost_left
               } else {
                  cost_right
               };
               *(cost_matrix
                  .get_mut(person_left)
                  .ok_or(())?
                  .get_mut(person_right)
                  .ok_or(())?) -= diff;
               *(cost_matrix
                  .get_mut(person_right)
                  .ok_or(())?
                  .get_mut(person_left)
                  .ok_or(())?) -= diff;
            }
         }
      }
   }
   let mut report = Vec::new();
   for (buyer, owers) in cost_matrix {
      for (ower, amount) in owers {
         if amount > 0.0f32 {
            report.push((buyer.clone(), ower, amount));
         }
      }
   }
   Ok(report)
}

#[component]
pub fn report(
   cx: Scope,
   people: ReadSignal<HashSet<String>>,
   expenses: ReadSignal<HashMap<String, Expense>>,
) -> impl IntoView {
   let (report, report_mut) = create_signal(cx, Vec::new());

   let target_currency = create_node_ref::<Select>(cx);

   let calculate = move |_| {
      let people = people.get();
      let currencies = CurrencyHandle::new();
      let target_currency = target_currency.get().unwrap().value();
      let curr = currencies.confirm_currency(target_currency).unwrap();
      report_mut.update(|report| {
         *report = calculate(
            &people,
            &expenses
               .get()
               .values()
               .map(|expense| expense.to_owned())
               .collect::<Vec<Expense>>(),
            &currencies,
            curr,
         )
         .unwrap()
      });
   };

   view!(cx,
       <p>"For currency:"</p>
       <Currencies value_ref=target_currency/>
       <button on:click=calculate>"Calculate Expenses"</button><br/>
       <br/>
       <ul style="text-align:left">
           {move||{report
               .get()
               .into_iter()
               .map(|line| view!(cx,
                   <li>{line.0}" will receive " {format!("{:.2}",line.2)}" from "{line.1}</li>
               ))
               .collect::<Vec<_>>()}}
           /*<For
               each = move || {report.get()}
               key = |(buyer,ower,_)| {(buyer.to_owned(),ower.to_owned())}
               view = move |cx,line| {
                   view!(cx,
                       <li>{line.0}" will receive " {format!("{:.2}",line.2)}" from "{line.1}</li>
                   )
               }
           />*/
       </ul>
   )
}

#[component]
pub fn report_help(cx: Scope) -> impl IntoView {
   view!(cx,
       <p>
           <b>"Report "</b>"- the final journey of this app. After providing all information you have, you can calculate, who needs to pay back whom in the currency of choice. For conveniance it will show optimized results (if for example Mark bought something more expensive to Adam, than Adam bought to Mark, Adam will return less money to Mark)."
       </p>
   )
}
