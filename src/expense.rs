mod modal;

use crate::expense::modal::{ExpenseDialog,ExpenseDialogProps, ExpenseModalType};
use leptos::{
   html::Dialog,
   *,
};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Expense {
   buyer: String,
   amount: f32,
   currency: String,
   buyer_included: bool,
   owers: Vec<String>,
}

impl Expense {
   pub fn new(buyer: String, amount: f32, currency: String) -> Expense {
      Expense {
         buyer,
         amount,
         currency,
         buyer_included: false,
         owers: Vec::new(),
      }
   }

   pub fn add_ower(&mut self, ower: String) {
      if self.buyer != ower {
         self.owers.push(ower);
      } else {
         self.buyer_included = true;
      }
   }

   pub fn print_me(&self) -> String {
      let owers_str = {
         let owers_str = self
            .owers
            .iter()
            .map(|ower| ower.to_string())
            .collect::<Vec<String>>()
            .join(", ");
         let c = owers_str.rfind(',').unwrap_or_else(|| 0);
         let (lead, last) = owers_str.split_at(c);

         let last = last.replace(",", " and");
         format!("{}{}", lead, last)
      };
      format!(
         "{} spent {:.2} {} for {}, {}cluding {}",
         self.buyer,
         self.amount,
         self.currency,
         owers_str,
         if self.buyer_included { "in" } else { "ex" },
         self.buyer
      )
   }

   pub fn get_buyer(&self) -> String {
      self.buyer.clone()
   }

   pub fn has_missing_people(&self, people: &HashSet<String>) -> bool {
      if people.contains(&self.buyer) {
         for ower in self.owers.iter(){
            if !people.contains(ower){ return true}
         }
         false
      }
      else{
         true
      }
   }

   pub fn get_currency(&self) -> String {
      self.currency.clone()
   }

   pub fn snapshot(&self) -> (String, f32, String, bool, Vec<String>) {
      (
         self.buyer.clone(),
         self.amount,
         self.currency.clone(),
         self.buyer_included,
         self.owers.clone(),
      )
   }

   pub fn calculate(&self, currency_factor: f32) -> HashMap<String, f32> {
      let mut report: HashMap<String, f32> = HashMap::new();
      let divider_expander: usize = self.buyer_included.into();
      let divided_amount = self.amount / (self.owers.len() + divider_expander) as f32;
      for ower in self.owers.iter() {
         report.insert(ower.to_owned(), divided_amount * currency_factor.recip());
      }
      report
   }
}

#[component]
pub fn expenses(
   cx: Scope,
   people: ReadSignal<HashSet<String>>,
   expenses_write: WriteSignal<HashMap<String, Expense>>,
   expenses_read: ReadSignal<HashMap<String, Expense>>,
) -> impl IntoView {

    let expense_modal_type = create_rw_signal(cx, ExpenseModalType::default());
    let dialog_ref = create_node_ref::<Dialog>(cx);

   // This prevents expenses from having records with people that do not exist
   create_effect(cx, move |_| {
      expenses_write.update(|expenses| {
         let mut expenses_to_delete = Vec::new();
         let people = people.get();
         for (name, expense) in expenses.iter() {
            if expense.has_missing_people(&people) {
               expenses_to_delete.push(name.clone());
            }
         }
         for name in expenses_to_delete {
            expenses.remove(&name);
         }
      })
   });

   create_effect(cx, move |_| {
       expense_modal_type.with(|modal_type|{
           match modal_type{
            ExpenseModalType::New { freeze, name, expense } => {
                if *freeze{
                    expenses_write.update(|expenses|{
                        expenses.insert(name.to_owned(), expense.to_owned());
                    })
                }
            },
            ExpenseModalType::NoType => {},
        }
       })
   });

   view! {cx,
        <p>"Current Expenses:"</p>
        <button on:click=move |_|{
            expense_modal_type.update(|modal_type|{
                *modal_type = ExpenseModalType::New{freeze: false, name: String::default(), expense: Expense::default() };
            });
            dialog_ref.get().unwrap().show_modal().unwrap();
        }>"New"</button><br/>
        <dialog _ref=dialog_ref>
            <ExpenseDialog modal_data=expense_modal_type people=people dialog_ref=dialog_ref/>
        </dialog>
        <ul style="width:50%;margin:auto">
           <For
                each=move || {expenses_read.get()}
                key=|(name,_)| {name.to_owned()}
               view = move |cx, (name,expense)| {
                   view! {cx,
                       <li>
                           <div style="display:flex;align-self:center;text-align:center">
                               {name.clone()}": "{expense.print_me()}<button on:click=move |_|{
                                   expenses_write.update(|expenses|{
                                       expenses.remove_entry(&name);
                                   })
                               } style="width:6rem;margin-left:auto">"Delete"</button>
                           </div>
                       </li>
                   }
               }
           />
       </ul>
   }
}


#[component]
pub fn expenses_help(cx: Scope) -> impl IntoView {
   view!(cx,
       <p>
           <b>"Expenses "</b>"- the biggest tab, because it requires the most information. In short - this is the tab where you define 'who bought what for who and for how much'."<br/>
           "At first you need to provide name for the expense - this is a mandatory step, because name identifies expense."<br/>
           "Next step is to select the person who paid for this expense, for how much in what currency."<br/>
           "Then select people, for which it was paid. This information is needed for money split. You can choose buyer here for including him/her in the split."<br/>
           "After this expense can be added."<br/>
           "Here you can also show already provided expense as the short sentence, as well as remove unwanted or invalid expenses."
       </p>
   )
}
