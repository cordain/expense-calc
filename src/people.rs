use std::collections::HashSet;

use leptos::{
   html::{Input, Select},
   *,
};

fn into_sorted(people: HashSet<String>) -> Vec<String> {
   let mut vec = people.into_iter().collect::<Vec<String>>();
   vec.sort_unstable();
   vec
}

#[component]
pub fn people_select(
   cx: Scope,
   people: ReadSignal<HashSet<String>>,
   val_ref: NodeRef<Select>,
   multiple: bool,
) -> impl IntoView {
   view!(cx,
       <select multiple=multiple id="owers" _ref=val_ref>
           <For
               each=move || {into_sorted(people.get())}
               key=|person| {person.to_owned()}
               view=move |cx, person| {
                   view! {cx,
                       <option value={person.clone()}>{person}</option>
                   }
               }
           />
       </select>
   )
}

#[component]
pub fn people(
   cx: Scope,
   people_read: ReadSignal<HashSet<String>>,
   people_write: WriteSignal<HashSet<String>>,
) -> impl IntoView {
   let name = create_node_ref::<Input>(cx);

   let add_person = move |_| {
      people_write.update(|people| {
         let mut name = name.get().unwrap().value();
         name.truncate(30);
         people.insert(name);
      });
   };

   view!(cx,
       <p class="text-center">"Add people"</p>
       <input type="text" _ref=name maxlength=30/>
       <button on:click=add_person>"Add"</button>
       <ul style="width:50%;margin:auto">
           <For
               each= move || {into_sorted(people_read.get())}
               key = |person| person.clone()
               view = move |cx, person| {
                   view! {cx,
                       <li>
                           <div style="display:flex;align-self:center;text-align:center">
                               {person.clone()}<button on:click=move |_|{
                                   people_write.update(|people|{
                                       people.take(&person);
                                   })
                               } style="width:6rem;margin-left:auto">"Delete"</button>
                           </div>
                       </li>
                   }
               }
           />
       </ul>
   )
}

#[component]
pub fn people_help(cx: Scope) -> impl IntoView {
   view!(cx,
       <p>
           <b>"People "</b>"- the first page you should visit, where you will add group members or removing them if you did a mistake when providing a valid name. The list of people created on this page will occur on the Expenses page"<br/>
           <b>"Note: "</b> "Deleting people here will affect already provided expenses. Keep it in mind and be sure to clean up bad names quite early."
       </p>
   )
}
