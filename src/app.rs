use std::collections::{HashMap, HashSet};

use crate::expense::Expense;
use crate::expense::{Expenses, ExpensesProps};
use crate::help::{Help, HelpProps};
use crate::people::{People, PeopleProps};
use crate::report::{Report, ReportProps};
use leptos::html::Div;
use leptos::*;
use serde::{Deserialize, Serialize};

enum StorageError {
   LoadStorage,
   NoStorage,
   LoadItem,
   NoItem,
   ItemSerde,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct CalculatorStorageItem {
   people: HashSet<String>,
   expense_list: HashMap<String, Expense>,
}

impl CalculatorStorageItem {
   pub fn split(self) -> (HashMap<String, Expense>, HashSet<String>) {
      (self.expense_list, self.people)
   }
}

fn get_calculator(name: String) -> Result<CalculatorStorageItem, StorageError> {
   let storage = window()
      .local_storage()
      .map_err(|_| StorageError::LoadStorage)?
      .ok_or(StorageError::NoStorage)?;
   serde_json::from_str::<CalculatorStorageItem>(
      storage
         .get_item(name.as_str())
         .map_err(|_| StorageError::LoadItem)?
         .ok_or(StorageError::NoItem)?
         .as_str(),
   )
   .map_err(|_| StorageError::ItemSerde)
}

fn set_calculator(
   name: String,
   people: HashSet<String>,
   expense_map: HashMap<String, Expense>,
) -> Result<(), StorageError> {
   let storage = window()
      .local_storage()
      .map_err(|_| StorageError::LoadStorage)?
      .ok_or(StorageError::NoStorage)?;
   let calculator = serde_json::to_string(&CalculatorStorageItem {
      people: people.clone(),
      expense_list: expense_map.clone(),
   })
   .map_err(|_| StorageError::ItemSerde)?;
   storage
      .set_item(name.as_str(), &calculator.as_str())
      .map_err(|_| StorageError::LoadItem)
}

#[component]
pub fn app(cx: Scope) -> impl IntoView {
   let (expense_list, people) = match get_calculator("default".to_string()) {
      Ok(calc) => calc.split(),
      Err(_) => (HashMap::new(), HashSet::new()),
   };

   let (people_read, people_write) = create_signal(cx, people);
   let (expense_list_read, expense_list_write) = create_signal(cx, expense_list);

   let people_tab_ref = create_node_ref::<Div>(cx);
   let expenses_tab_ref = create_node_ref::<Div>(cx);
   let report_tab_ref = create_node_ref::<Div>(cx);
   let help_tab_ref = create_node_ref::<Div>(cx);

   create_effect(cx, move |_| {
      set_calculator(
         "default".to_string(),
         people_read.get(),
         expense_list_read.get(),
      )
   });
   let reset_shown_tab = move || {
      people_tab_ref.get().unwrap().set_hidden(true);
      expenses_tab_ref.get().unwrap().set_hidden(true);
      report_tab_ref.get().unwrap().set_hidden(true);
      help_tab_ref.get().unwrap().set_hidden(true);
   };

   let show_expenses_tab = move |_| {
      reset_shown_tab();
      expenses_tab_ref.get().unwrap().set_hidden(false);
   };
   let show_people_tab = move |_| {
      reset_shown_tab();
      people_tab_ref.get().unwrap().set_hidden(false);
   };
   let show_report_tab = move |_| {
      reset_shown_tab();
      report_tab_ref.get().unwrap().set_hidden(false);
   };
   let show_help_tab = move |_| {
      reset_shown_tab();
      help_tab_ref.get().unwrap().set_hidden(false);
   };

   view!(cx,
       <div style="position:fixed; top:15%; width:100%; height:75%; overflow:auto">
           <div _ref=help_tab_ref style="text-align:justify"><Help/></div>
           <div _ref=people_tab_ref hidden><People people_read=people_read people_write=people_write/></div>
           <div _ref=expenses_tab_ref hidden><Expenses people=people_read expenses_write=expense_list_write expenses_read=expense_list_read /></div>
           <div _ref=report_tab_ref hidden><Report people=people_read expenses=expense_list_read/></div>
       </div>
       <div style="display:flex; position:fixed; bottom:0%; width:100%; height:10%">
           <button style="margin-bottom:0" on:click=show_people_tab>"People"</button>
           <button style="margin-bottom:0" on:click=show_expenses_tab>"Expenses"</button>
           <button style="margin-bottom:0" on:click=show_report_tab>"Report"</button>
           <button style="margin-bottom:0" on:click=show_help_tab>"Help"</button>
           <button style="margin-bottom:0" on:click=move |_| {
               expense_list_write.update(|expenses| {*expenses = HashMap::new()});
               people_write.update(|people| {*people = HashSet::new()});
           }>"Clear Data"</button>
       </div>
   )
}
