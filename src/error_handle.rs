use std::fmt::Display;

pub fn pass_error<R, E>(e: Result<R, E>) -> Result<R, E>
where
   E: Display + Clone,
   R: Clone,
{
   if let Err(e) = e.clone() {
      crate::alert(&e.to_string());
   }
   e
}
