use crate::{
   expense::{ExpensesHelp, ExpensesHelpProps},
   people::{PeopleHelp, PeopleHelpProps},
   report::{ReportHelp, ReportHelpProps},
};
use leptos::*;

#[component]
pub fn help(cx: Scope) -> impl IntoView {
   view!(cx,
       <p>"Welcome to the website, where you can collect all the expenses that you and your cooperatives made."<br/>"Maybe you wonder, how could you even start with using this website. Let me show you!"</p>
       <p>"On the bottom you can see basic tabs that moves you though the different parts of the application. Starting from the left:"</p>
       <ul>
           <li><PeopleHelp/></li>
           <li><ExpensesHelp/></li>
           <li><ReportHelp/></li>
           <li><HelpHelp/></li>
           <li><p><b>"Clear Data "</b>"- Button for clearing all of provided people and expenses. "<br/><b>"Note: "</b>"This is not a tab. It will immediately clear all the data. Watch for it!"</p></li>
       </ul>
       <p>"If you aren't scared of this block of text so far, then..."</p>
       <p>"Have fun!"</p>
   )
}

#[component]
pub fn help_help(cx: Scope) -> impl IntoView {
   view!(cx,
       <p>
           <b>"Help "</b>"- Content of this tab. Convenient if you wish to go back for tips."
       </p>
   )
}
