use leptos::{html::Select, *};
use phf::{phf_map, Map};

use serde_derive::*;
include!(concat!(env!("OUT_DIR"), "/env.rs"));

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Currency {
   pub currency_name: &'static str,
   pub rate: f32,
   pub rate_for_amount: f32,
}

impl Default for Currency {
   fn default() -> Self {
      Self {
         currency_name: "NCV",
         rate: 1.0,
         rate_for_amount: 1.0,
      }
   }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct CurrencyList {
   status: String,
}

#[derive(Debug, Clone)]
pub struct CurrencyHandle;

impl CurrencyHandle {
   pub fn new() -> CurrencyHandle {
      CurrencyHandle
   }

   pub fn into_currency_map(self) -> Vec<(&'static str, &'static str)> {
      let mut keys = CURRENCY_MAP.keys().map(|key| *key).collect::<Vec<&str>>();
      keys.sort_unstable();
      keys
         .into_iter()
         .map(|currency| (currency, CURRENCY_MAP[currency].currency_name))
         .collect()
   }

   pub fn confirm_currency(&self, currency: String) -> Option<String> {
      CURRENCY_MAP.contains_key(&currency[..]).then(|| currency)
   }

   pub fn get_rate(&self, currency: String) -> f32 {
      CURRENCY_MAP[currency.as_str()].rate
   }
}

#[component]
pub fn currencies(cx: Scope, value_ref: NodeRef<Select>) -> impl IntoView {
   view!(cx,
       <select _ref=value_ref>
           <For
               each=move || { CurrencyHandle::new().into_currency_map() }
               key=|currency| {currency.to_owned()}
               view=move |cx,currency|{
                   view!{cx,
                       <option value={currency.0}>{currency.1}</option>
                   }
               }
           />
       </select>
   )
}
