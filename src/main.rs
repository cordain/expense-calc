mod app;
mod currency;
mod error_handle;
mod expense;
mod help;
mod people;
mod report;
mod utils;

use app::{App, AppProps};

use leptos::*;
use wasm_bindgen::prelude::wasm_bindgen;

pub fn main() {
   mount_to_body(|cx| view! { cx,  <App /> })
}

#[wasm_bindgen]
extern "C" {
   pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
   alert("Hello, expense-calc!");
}
