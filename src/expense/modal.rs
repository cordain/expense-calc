use std::collections::HashSet;

use leptos::*;
use leptos::html::{Input, Select, Dialog};
use crate::expense::Expense;
use crate::{
   currency::{Currencies, CurrenciesProps},
   people::{PeopleSelect, PeopleSelectProps},
};

#[derive(Clone,Default)]
pub enum ExpenseModalType{
    New{freeze: bool, name: String, expense: Expense},
    #[default]
    NoType,
}

#[component]
pub fn expense_dialog(cx: Scope, modal_data: RwSignal<ExpenseModalType>, people: ReadSignal<HashSet<String>>, dialog_ref: NodeRef<Dialog>) -> impl IntoView {
   let name = create_node_ref::<Input>(cx);
   let buyer = create_node_ref::<Select>(cx);
   let amount = create_node_ref::<Input>(cx);
   let expense_currency = create_node_ref::<Select>(cx);
   let owers = create_node_ref::<Select>(cx);

   let submit = move |_| {
       let input_modal_data = modal_data.get();
       modal_data.update(|modal_data|{
           *modal_data = match input_modal_data{
               ExpenseModalType::New { .. } => {
                   if let Ok(amount) = amount.get().unwrap().value().parse(){

                       let mut new_expense = Expense::new(
                           buyer.get().unwrap().value().into(),
                           amount,
                           expense_currency.get().unwrap().value(),
                           );
                       let ower_nodes = owers.get().unwrap().selected_options();
                       for i in 0..ower_nodes.length() {
                           if let Some(ower) = people.get().get(&ower_nodes.item(i).unwrap().inner_html()) {
                               new_expense.add_ower(ower.to_owned());
                           }
                       }
                       let mut new_name = name.get().unwrap().value();
                       new_name.truncate(100);
                       ExpenseModalType::New{freeze: !new_name.is_empty(), name: new_name, expense: new_expense}
                   }
                   else{    
                       ExpenseModalType::New{freeze: false, name: String::default(), expense: Expense::default()}
                   }
               },
               _ => {ExpenseModalType::NoType}
           };
       });

       dialog_ref.get().unwrap().close();
   };


   view!(cx,
       <div style="display:flex; flex-direction:column">
       <p>"Name the expense:"</p>
       <input type="text" _ref=name maxlength="100" placeholder="Cinema tickets for 'The movie'."/>
       <p >"Who bought:"</p>
       <PeopleSelect people=people val_ref=buyer multiple=false/><br/>
       <p >"For how much:"</p>
       <div style="display:flex; flex-direction:row">
       <input type="number" _ref=amount placeholder=100/>
       <Currencies value_ref=expense_currency/>
       </div>
       //<div style="display:flex; flex-direction:column">
       <p >"Who owes for this:"</p>
       <PeopleSelect people=people val_ref=owers multiple=true/><br/><br/>
       <button on:click=submit>"Add Expense"</button><br/>
       </div>
   )
}
