# Expense Calculator

This project contains code for expense calculator website hosted on [gitlab pages](https://cordain.gitlab.io/expense-calc/).

Underlying logic is written in Rust and compiled to webassembly. Webpage is build on static documents, so it can be hosted on Github Pages.

# How to use

todo!();

# How to develop

- Clone the repo
```bash
git clone https://gitlab.com/Cordain/expense-calc.git
```

- Update your API key in `.cargo/config.toml` from [Currency Api Website](https://currency.getgeoapi.com) in env.rs
```rust
CURAPIKEY="Please add your API key here";
```

- Serve site with trunk
```bash
trunk serve --dist public/
```

- Modify code and show it instantly on the web browser
- Drink coffee
- Eat cookies
